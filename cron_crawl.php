<?php
require_once("create_video.php");


add_action("init", "cron_crawl");
function cron_crawl(){
	global $wpdb;
	if (isset($_GET['cron_crawl'])){
		$channels = $wpdb->get_results("SELECT * FROM `wp_pcs_youtube_channel`"); 
		foreach($channels as $channel){
			$rss = file_get_contents("https://www.youtube.com/feeds/videos.xml?channel_id=".$channel->channel_id);
			$data = simplexml_load_string($rss);
			$i = 0;
			foreach($data->entry as $entry){
				$i ++;
				// if ($i > 5) break;
				$id = (string) $entry->id;
				$id = str_replace("yt:video:", "", $id);
				$checkStatus = $wpdb->get_var("SELECT * FROM `wp_postmeta` WHERE `meta_value` LIKE '%$id%'");
				if ($checkStatus > 0){
					break;
				}else{
					createMovie($id, "post", [$channel->category_id]);
				}
			}
		}
	}

}
