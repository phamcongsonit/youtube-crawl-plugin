<?php
/**
 * Register a custom menu page.
 */
function wpdocs_register_my_custom_menu_page(){
    add_menu_page( 
        __( 'Youtube Crawl', 'textdomain' ),
        'Youtube Crawl',
        'manage_options',
        'youtube_crawl',
        'my_custom_menu_page'
    ); 
}
add_action( 'admin_menu', 'wpdocs_register_my_custom_menu_page' );
 
/**
 * Display a custom menu page
 */

function my_custom_menu_page(){
	if (isset($_POST['crawl'])){
		$youtubeID = explode("?v=", $_POST['youtube_url'])[1];
		$id = createMovie($youtubeID, $_POST['post_type'], $_POST['category']);
	}
	if (isset($_GET['page']) && $_GET['page'] == 'youtube_crawl' && !isset($_GET['action'])):
		if (isset($_GET['delete'])){
			global $wpdb;
			$wpdb->query("DELETE FROM `wp_pcs_youtube_channel` WHERE `id` = ".$_GET['delete']);
			$urlBack = remove_query_arg("delete");
			header("Location: ".$urlBack);
		}
	?>
	<div class="wrap">
		<h1 class="wp-heading-inline">Youtube Crawl</h1>
		<?php if (isset($_POST['crawl']) && $id != null): ?>
		<div class="notice notice-success is-dismissible">
	        <p><?php _e( 'Crawl complete!'); ?></p>
	    </div>
	    <?php elseif (isset($_POST['crawl']) && $id == null): ?>
	    <div class="notice notice-error is-dismissible">
	        <p><?php _e( 'Crawl error!'); ?></p>
	    </div>
		<?php endif; ?>
		<div class="form-wrap " style="width:30%;float:left">
			<form action="#" method="POST">
				<div class="form-group">
					<label for="">Youtube URL: </label>
					<input type="text" name="youtube_url" required >
				</div>
				<div class="form-group">
					<label for="">Post Type: </label>
					<select name="post_type" id="" required>
						<!-- <?php foreach(get_post_types() as $postType): ?>
							<option value="<?=$postType?>"><?=$postType?></option>
						<?php endforeach; ?> -->
						<option value="post" default>Movies</option>
						<!-- <option value="tvshows">TV-Series</option> -->
						<option value="episodes">Episodes</option>
					</select> 

				</div>
				<div class="form-group category">
					<label for="">Category: </label>
					<?php foreach(get_categories(["hide_empty" => 0]) as $category):?>
						<input type="checkbox" name="category[]" value="<?=$category->term_id?>">
						<?=$category->name?> 
						<br/>
					<?php endforeach; ?>
				</div>
				<br/>
				<div class="form-group">
					<button name="crawl" class="button button-primary">Crawl</button>
				</div>
			</form>
		</div>
		<div class="table data" style="float:left;width:70%">
			<?php $current_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
			<a href="<?=add_query_arg('action', 'add_channel', $current_url);?>" class="button button-primary">Thêm kênh mới</a>
			<div style="padding-top:20px"></div>
			<table class="table" >
				<thead>
					<tr>
						<th>ID</th>
						<th>Tên kênh</th>
						<th>ID kênh</th>
						<th>Chuyên mục</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
					global $wpdb;
					$channels = $wpdb->get_results("SELECT * FROM `wp_pcs_youtube_channel`"); 
					foreach($channels as $channel): 
					?>
					<tr>
						<td><?=$channel->id?></td>
						<td><a href="<?=$channel->url?>"><?=$channel->name?></a></td>
						<td><?=$channel->channel_id?></td>
						<td><?=get_the_category_by_ID($channel->category_id)?></td>
						<td><a href="<?=add_query_arg('delete', $channel->id, $current_url);?>">Xóa</a></td>
					</tr>
					<?php
					endforeach;
					?>
				</tbody>
			</table>
		</div>
	</div>
	<script type="text/javascript">
		jQuery(function(){
			$ = jQuery
			$("select[name=post_type]").change(function(){
				postType = $(this).val();
				if (postType == 'post'){
					$(".category").show();
				}else{
					$(".category").hide();
				}
			})
			$(document).ready( function () {
			    $('table.table').DataTable();
			} );
		})
	</script>
	<?php 

	elseif (isset($_GET['page']) && $_GET['page'] == 'youtube_crawl' && $_GET['action'] == 'add_channel'):
		if (isset($_POST['url_channel'])){
			$html = file_get_contents($_POST['url_channel']);
			preg_match("/itemprop=\"channelId\" content=\"(.*)\"/U", $html, $idChannel);
			preg_match("/itemprop=\"name\" content=\"(.*)\"/U", $html, $name);
			if (isset($idChannel[1])){
				$idChannel = $idChannel[1];
				$name = $name[1];
				$urlBack = remove_query_arg("action");
				global $wpdb;
				$wpdb->insert("wp_pcs_youtube_channel", [
					"url" => $_POST['url_channel'],
					"name" => $name,
					"channel_id" => $idChannel,
					"category_id" => $_POST['category_id']
				], ["%s","%s","%s", "%s"]);	
				header("Location: $urlBack");
				exit;
			}
		}
		?>
			<div class="wrap">
				<h1 class="wp-heading-inline">Thêm kênh mới</h1>
				<form action="#" method="POST">
					<div style="padding:10px"></div>
					<label for="">URL Kênh</label>
					<div style="padding:5px"></div>
					<input type="text" name="url_channel"><br/>
					<div style="padding:5px"></div>
					<label for="">Chuyên mục</label>
					<div style="padding:5px"></div>
					<select name="category_id" id="">
						<?php
						foreach(get_categories(['hide_empty'        => 0]) as $category){
							echo "<option value='".$category->term_id."'>".$category->name."</option>";
						}
						?>
					</select>
					<br/>
					<div style="padding:5px"></div>
					<button class="button button-primary">Thêm</button>
				</form>
			</div>
		<?php
	endif;
}

wp_enqueue_style ('datatable-style', 'https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css');
wp_enqueue_script ('datatable-script', 'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js');
