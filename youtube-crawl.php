<?php
/*
Plugin Name: Youtube Crawl
Description: Crawl Youtube Information 
Version:     1.2
Author:      Pham Cong Son
Author URI:  http://phamcongson.com
Bitbucket Plugin URI: https://bitbucket.org/phamcongsonit/youtube-crawl-plugin
*/	

require_once("register_menu.php");
require_once("create_video.php");
require_once("cron_crawl.php");

// SETUP CRON
add_action('wp', 'myplugin_schedule_cron');
function myplugin_schedule_cron() {
  if ( !wp_next_scheduled( 'myplugin_cron' ) )
    wp_schedule_event(time(), 'daily', 'myplugin_cron');
}
// the CRON hook for firing function
add_action('myplugin_cron', 'myplugin_cron_function');
#add_action('wp_head', 'myplugin_cron_function'); //test on page load

// the actual function
function myplugin_cron_function() {
    // see if fires via email notification
    file_put_contents(__DIR__.'/time', time());
}

function create_plugin_database_table()
{
    global $table_prefix, $wpdb;

    $tblname = 'pcs_youtube_channel';
    $wp_track_table = $table_prefix . "$tblname";
    if($wpdb->get_var( "show tables like '$wp_track_table'" ) != $wp_track_table) 
    {	
    	$sql = <<<EOF
    	CREATE TABLE `$wp_track_table` (
    	  `id` int(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    	  `url` varchar(225) NOT NULL,
    	  `name` varchar(225) NOT NULL,
    	  `channel_id` varchar(225) NOT NULL,
    	  `category_id` varchar(225) NOT NULL
    	) ;
EOF;
        require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
        dbDelta($sql);
    }

}

register_activation_hook( __FILE__, 'create_plugin_database_table' );