<?php
require_once( ABSPATH . 'wp-admin/includes/image.php' );
	
function createMovie($youtubeID, $postType, $categoryID){
	$data = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=id,snippet&id=".$youtubeID."&key=AIzaSyA12yVXpKLZQOdj1H-d8clpeLZNQT4sH5k");
	$data = json_decode($data, true);
	$videoID = $data['items'][0]['id'];
	$title = $data['items'][0]['snippet']['title'];
	$tags = $data['items'][0]['snippet']['tags'];
	$description = $data['items'][0]['snippet']['description'];
	$description .= "\n".implode(", ", $tags);
	$thumbnail = $data['items'][0]['snippet']['thumbnails']['high']['url'];
	if (isset($data['items'][0]['snippet']['thumbnails']['maxres'])){
		$thumbnail = $data['items'][0]['snippet']['thumbnails']['maxres']['url'];
	}
	// var_dump($data['items'][0]['snippet']);
	if ($postType == 'post'){
		$categoryID = $categoryID;
	}elseif ($postType == 'episodes'){
		$categoryID = [];
	}
	$newPost = array(
		'ID' => '',
		'post_type' => $postType,
		'post_author' => 1, 
		'post_category' => $categoryID,
		'post_content' => $description, 
		'post_title' => $title,
		'tags_input' => $tags,
		'post_status' => 'publish'
	);
	$id = wp_insert_post($newPost, true);
	add_post_meta($id, "player_0_name_player", "YTB");
	add_post_meta($id, "_player_0_name_player", "field_5a6ae00d1df8f");
	add_post_meta($id, "player_0_type_player", "p_embed");
	add_post_meta($id, "_player_0_type_player", "field_591fd3cc1c291");
	add_post_meta($id, "player_0_quality_player", "HD 1080p");
	add_post_meta($id, "_player_0_quality_player", "field_5640cc8323220");
	add_post_meta($id, "player_0_embed_player", '<iframe width="560" height="315" src="https://www.youtube.com/embed/'.$videoID.'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>');
	add_post_meta($id, "_player_0_embed_player", "field_5640cc9823221");
	add_post_meta($id, "player", "1");
	add_post_meta($id, "_player", "field_5640ccb223222");
	// set thumbnail
	$uploaddir = wp_upload_dir();
	$filename = $uploaddir['path'] . '/' . uniqid().".jpg";
	$contents= file_get_contents($thumbnail);
	$savefile = fopen($filename, 'w');
	fwrite($savefile, $contents);
	fclose($savefile);
	$filetype = wp_check_filetype( basename( $filename ), null );
	$wp_upload_dir = wp_upload_dir();
	$attachment = array(
		'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
		'post_mime_type' => $filetype['type'],
		'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
		'post_content'   => '',
		'post_status'    => 'inherit'
	);

	$attach_id = wp_insert_attachment( $attachment, $filename, $id );


	$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
	wp_update_attachment_metadata( $attach_id, $attach_data );
	set_post_thumbnail( $id, $attach_id );
	return $id;
}
